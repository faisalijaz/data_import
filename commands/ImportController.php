<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/25/18
 * Time: 10:50 AM
 */

namespace app\commands;

use app\models\CsvData;
use yii\console\Controller;

class ImportController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        // $date = date('Y-m-d', strtotime("-1 days"));
        $date = '2018-10-15';
        // $countries = Countries::find()->all();


        $path = 'http://csv.import/uploads/' . $date . '/country-specific-database/oman.csv';
        $contents = fopen($path, "r");
        $index = fgetcsv($contents, 1000, ",");

        $data = [];
        $duplicates = 0;

        while (($filesop = fgetcsv($contents, 1000, ",")) !== false) {

            $csv_model = new CsvData();
            $csv = [];

            $csv['CsvData']['user_id'] = 0;

            $csv['CsvData']['name'] = "";
            if (isset($filesop[10])) {
                $csv['CsvData']['name'] = $filesop[10];
            }

            $csv['CsvData']['email'] = "no-email@xyz.com";
            if (!empty($filesop[17])) {
                if (isset($filesop[17])) {
                    $csv['CsvData']['email'] = $filesop[17];
                }
            }

            $csv['CsvData']['phone_number'] = "123456";
            if (!empty($filesop[18])) {
                if (isset($filesop[18])) {
                    $csv['CsvData']['phone_number'] = $filesop[18];
                }
            }

            if (isset($filesop[1])) {
                $csv['CsvData']['domain'] = $filesop[1];
            }

            if (isset($filesop[11])) {
                $csv['CsvData']['compnay'] = $filesop[11];
            }

            if (isset($filesop[13])) {
                $csv['CsvData']['city'] = $filesop[13];
            }

            if (isset($filesop[2])) {
                $csv['CsvData']['date'] = date('Y-m-d', strtotime($filesop[2]));
            }

            if (isset($filesop[3])) {
                $csv['CsvData']['registration_date'] = date('Y-m-d', strtotime($filesop[3]));
            }

            if (isset($filesop[5])) {
                $csv['CsvData']['expiry_date'] = date('Y-m-d', strtotime($filesop[5]));
            }

            if (isset($filesop[50])) {
                $csv['CsvData']['server'] = $filesop[50];
            }

            if (isset($filesop[54])) {
                $csv['CsvData']['ip_address'] = $filesop[54];
            }

            if (isset($filesop[16])) {
                $csv['CsvData']['country'] = $filesop[16];
            }

            if (isset($filesop[7])) {
                $csv['CsvData']['category'] = $filesop[7];
            }

            if (isset($filesop[8])) {
                $csv['CsvData']['user_id'] = $filesop[8];
            }

            if (isset($filesop[9])) {
                $csv['CsvData']['source'] = $filesop[9];
            }

            if (isset($filesop[55])) {
                $csv['CsvData']['status'] = $filesop[55];
            }

            $csv_model->load($csv);

            if (!$csv_model->save()) {

                if (count($csv_model->getErrors()) > 0) {

                    $mapped = false;

                    foreach ($csv_model->getErrors() as $error) {
                        if (is_array($error) && count($error) > 0) {
                            foreach ($error as $err) {
                                if ($err == "Email must be unique." || $err == "Phone number must be unique.") {
                                    if (!$mapped) {

                                        $duplicates = 1;
                                        $id = CsvData::find()->where(['email' => $csv['CsvData']['email']])->one();
                                        if ($id <> null) {
                                            $csv['CsvData']['data_id'] = $id->id;
                                        }

                                        $data[] = $csv['CsvData'];
                                        $mapped = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        fclose($contents);

    }

}