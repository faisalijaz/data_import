<?php

namespace app\controllers;

use app\models\CsvData;
use app\models\CsvDataSearch;
use app\models\Logs;
use app\models\SubData;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DataController implements the CRUD actions for CsvData model.
 */
class DataController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsvData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsvDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsvData model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CsvData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CsvData();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CsvData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CsvData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CsvData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CsvData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CsvData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     */
    public function actionLogs($id)
    {
        if (Yii::$app->request->isAjax) {

            $searchModel = new CsvDataSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionMergeDuplicates()
    {
        if (\Yii::$app->session->get('duplicates')) {

            $message = \Yii::$app->session->getFlash('duplicate_msg');
            $data = \Yii::$app->session->get('duplicates');

            return $this->render('duplicates', [
                'model' => $data,
                'message' => $message
            ]);
        }
    }


    /**
     * @return string
     */
    public function actionSaveDuplicates()
    {
        if (Yii::$app->request->post()) {

            $data = \Yii::$app->session->get('duplicates');

            $array = [];
            $errors = [];

            if (count($data) > 0) {

                foreach ($data as $record) {

                    $model = new SubData();
                    $array['SubData'] = $record;

                    $model->load($array);
                    if (!$model->save()) {
                        $errors[] = $model->getErrors();
                    }
                }

                if (count($errors) > 0) {
                    Yii::$app->session->setFlash('errors', $errors);
                } else {
                    Yii::$app->session->setFlash('success', "Record Merged");
                }

                \Yii::$app->session->set('duplicates', null);
                $this->redirect(['/data']);

            }

        } else {

            \Yii::$app->session->setFlash('duplicate_msg', 'Some Message!');
            $this->redirect(['/data']);

        }
    }

    /**
     * @param $id
     * @param string $type
     * @return string
     */
    public function actionSaveLogs($id, $type = "main")
    {
        $model = new Logs();
        $model->data_id = $id;
        $model->type = $type;

        $chats = Logs::find()->where(['data_id' => $id])
            ->andWhere(['type' => $type])
            ->orderBy([
                'id' => SORT_DESC
            ])->all();

        return $this->renderAjax('_logs', [
            'model' => $model,
            'logs' => $chats,
            'id' => $id,
            'type' => $type
        ]);

    }


    public function actionSubmitLogs($id, $type = "main")
    {
        $model = new Logs();
        $model->data_id = $id;
        $model->type = $type;

        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());

            if ($model->save()) {

                if ($type == "main") {
                    $main_data = CsvData::find()->where(['id' => $id])->one();
                } else {
                    $main_data = SubData::find()->where(['id' => $id])->one();
                }

                $main_data->isNew = 0;
                $main_data->interests = $model->interests;
                $main_data->save();

                Yii::$app->session->setFlash('success', 'Logs saved');
            } else {
                Yii::$app->session->setFlash('error', 'Log not saved');
            }

        }

        $chats = Logs::find()->where(['data_id' => $id])
            ->andWhere(['type' => $type])
            ->orderBy([
                'id' => SORT_DESC
            ])->all();

        $html = "";
        if ($chats <> null) {
            foreach ($chats as $chat) {

                $agenda = "";
                $date_time = "";
                $venue = "";

                if ($chat->agenda) {
                    $agenda = '<strong>Agenda: </strong>' . $chat->agenda . '<br/>';
                }

                if ($chat->date_time && $chat->date_time != "0000-00-00 00:00:00") {
                    $date_time = '<strong>Time: </strong>' . $chat->date_time . '<br/>';
                }

                if ($chat->venue) {
                    $venue = '<strong>Venue: </strong>' . $chat->venue . '<br/>';
                }


                $html .= '<div class="box-comment">
                            <div class="comment-text">
                                    <span class="username">
                                         <i class="fa fa-comment"></i>&nbsp;' . $chat->comments . '<br/>
                                        <span class="text-muted pull-right">
                                            ' . date("D M j G:i:s", strtotime($chat->date_comment)) . '
                                        </span>
                                    </span><br/>
                                    <p>' . $agenda . $date_time . $venue . '</p>
                            </div>
                            <!-- /.comment-text -->
                        </div>';


            }
        }

        echo $html;
        return;
    }

    /**
     *
     */
    public function actionUpdateStatus()
    {

        if (Yii::$app->request->post()) {

            $type = Yii::$app->request->post('type');

            if ($type == "main") {

                $model = CsvData::findOne(Yii::$app->request->post('id'));
                $model->status = Yii::$app->request->post('status');

                if (!$model->save()) {

                    echo 0;
                    return;
                }

                echo 1;
                return;
            }

            if ($type == "sub") {

                $model = SubData::findOne(Yii::$app->request->post('id'));
                $model->status = Yii::$app->request->post('status');

                if (!$model->save()) {

                    echo 0;
                    return;
                }

                echo 1;
                return;
            }

        }
    }

    /**
     *
     */
    public function actionUpdateSource()
    {

        if (Yii::$app->request->post()) {

            $model = CsvData::findOne(Yii::$app->request->post('id'));
            $model->source = Yii::$app->request->post('source');

            if (!$model->save()) {

                echo 0;
                return;
            }

            echo 1;
            return;

        }
    }

    /**
     *
     */
    public function actionUpdateSourceSub()
    {

        if (Yii::$app->request->post()) {

            $model = SubData::findOne(Yii::$app->request->post('id'));
            $model->source = Yii::$app->request->post('source');

            if (!$model->save()) {

                echo 0;
                return;
            }

            echo 1;
            return;

        }
    }

    public function actionChangeSource($id, $type)
    {

        if ($id) {

            if ($type == 'main') {
                $data = CsvData::find()->where(['id' => $id])->one();
            } else {
                $data = SubData::find()->where(['id' => $id])->one();
            }

            return $this->renderPartial('_change_source', [
                'model' => $data,
            ]);
        }

    }

    public function actionChangeStatus($id, $type)
    {

        if ($id) {

            if ($type == 'main') {
                $data = CsvData::find()->where(['id' => $id])->one();
            } else {
                $data = SubData::find()->where(['id' => $id])->one();
            }

            return $this->renderPartial('_change_status', [
                'model' => $data,
            ]);
        }

    }
}