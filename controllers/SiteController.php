<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\CsvData;
use app\models\LoginForm;
use app\models\LogsSearch;
use app\models\UploadForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LogsSearch();
        $searchModel->current_date = true;
        $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return string
     */
    public function actionImport()
    {
        $model = new UploadForm();

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');


            if ($model->file) {

                $filePath = 'uploads/' . $model->file->baseName . '.' . $model->file->extension;

                if ($model->file->saveAs($filePath)) {
                    $model->file = $filePath;
                }
            }

            return $this->render('mapping', [
                'filepath' => $filePath
            ]);

        }

        return $this->render('upload_file', [
            'model' => $model
        ]);
    }

    public function actionSaveDataImport()
    {

        if (Yii::$app->request->post()) {

            ini_set('memory_limit', '5024M');
            ini_set('max_execution_time', 2000);
            set_time_limit(0);

            $post = Yii::$app->request->post();

            $contents = fopen(Yii::$app->request->post('filePath'), "r");

            $data = [];

            $duplicates = 0;

            while (($filesop = fgetcsv($contents, 1000, ",")) !== false) {

                $csv_model = new CsvData();
                $csv = [];

                $csv['CsvData']['user_id'] = 0;
                if (isset($post['name'])) {
                    $csv['CsvData']['name'] = "";
                    if (isset($filesop[$post['name']])) {
                        $csv['CsvData']['name'] = $filesop[$post['name']];
                    }
                }

                if (isset($post['email'])) {

                    $csv['CsvData']['email'] = "no-email@xyz.com";

                    if (!empty($filesop[$post['email']])) {
                        if (isset($filesop[$post['email']])) {
                            $csv['CsvData']['email'] = $filesop[$post['email']];
                        }
                    }
                }

                if (isset($post['phone_number'])) {

                    $csv['CsvData']['phone_number'] = "123456";

                    if (!empty($filesop[$post['phone_number']])) {
                        if (isset($filesop[$post['phone_number']])) {
                            $csv['CsvData']['phone_number'] = $filesop[$post['phone_number']];
                        }
                    }
                }

                if (isset($post['domain'])) {
                    if (isset($filesop[$post['domain']])) {
                        $csv['CsvData']['domain'] = $filesop[$post['domain']];
                    }
                }

                if (isset($post['compnay'])) {
                    if (isset($filesop[$post['compnay']])) {
                        $csv['CsvData']['compnay'] = $filesop[$post['compnay']];
                    }
                }

                if (isset($post['city'])) {
                    if (isset($filesop[$post['city']])) {
                        $csv['CsvData']['city'] = $filesop[$post['city']];
                    }
                }

                if (isset($post['date'])) {
                    if (isset($filesop[$post['date']])) {
                        $csv['CsvData']['date'] = date('Y-m-d', strtotime($filesop[$post['date']]));
                    }
                }

                if (isset($post['registration_date'])) {
                    if (isset($filesop[$post['registration_date']])) {
                        $csv['CsvData']['registration_date'] = date('Y-m-d', strtotime($filesop[$post['registration_date']]));
                    }
                }

                if (isset($post['expiry_date'])) {
                    if (isset($filesop[$post['expiry_date']])) {
                        $csv['CsvData']['expiry_date'] = date('Y-m-d', strtotime($filesop[$post['expiry_date']]));
                    }
                }

                if (isset($post['server'])) {
                    if (isset($filesop[$post['server']])) {
                        $csv['CsvData']['server'] = $filesop[$post['server']];
                    }
                }

                if (isset($post['ip_address'])) {
                    if (isset($filesop[$post['ip_address']])) {
                        $csv['CsvData']['ip_address'] = $filesop[$post['ip_address']];
                    }
                }

                if (isset($post['country'])) {
                    if (isset($filesop[$post['country']])) {
                        $csv['CsvData']['country'] = $filesop[$post['country']];
                    }
                }

                if (isset($post['category'])) {
                    if (isset($filesop[$post['category']])) {
                        $csv['CsvData']['category'] = $filesop[$post['category']];
                    }
                }

                if (isset($post['user_id'])) {
                    if (isset($filesop[$post['user_id']])) {
                        $csv['CsvData']['user_id'] = $filesop[$post['user_id']];
                    }
                }

                if (isset($post['source'])) {
                    if (isset($filesop[$post['source']])) {
                        $csv['CsvData']['source'] = $filesop[$post['source']];
                    }
                }

                if (isset($post['status'])) {
                    if (isset($filesop[$post['status']])) {
                        $csv['CsvData']['status'] = $filesop[$post['status']];
                    }
                }


                $csv_model->load($csv);

                if (!$csv_model->save()) {

                    if (count($csv_model->getErrors()) > 0) {

                        $mapped = false;

                        foreach ($csv_model->getErrors() as $error) {

                            if (is_array($error) && count($error) > 0) {

                                foreach ($error as $err) {

                                    if ($err == "Email must be unique." || $err == "Phone number must be unique.") {

                                        if (!$mapped) {

                                            $duplicates = 1;

                                            $id = CsvData::find()->where(['email' => $csv['CsvData']['email']])->one();

                                            if ($id <> null) {
                                                $csv['CsvData']['data_id'] = $id->id;
                                            }

                                            $data[] = $csv['CsvData'];
                                            $mapped = true;

                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {

                }

                // unset($filesop);//
                // sleep(2);
            }

            fclose($contents);

            if (count($data) > 0 && $duplicates) {

                \Yii::$app->session->setFlash('duplicate_msg', "You have some duplicate data in the list. Do you wish to merge this?");
                \Yii::$app->session->set('duplicates', $data);

                $this->redirect(['/data/merge-duplicates']);

            } else {

                $this->redirect(['/data']);

            }
        }
    }
}
