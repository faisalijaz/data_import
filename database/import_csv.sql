-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 03, 2018 at 06:55 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `import_csv`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `csv_name` varchar(500) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `csv_name`, `status`) VALUES
(1, 'United Arab Emirates', 'united_arab_emirates.csv', '1'),
(2, 'Bahrain', 'bahrain.csv', '1'),
(3, 'Qatar', 'qatar.csv', '1'),
(4, 'Kuwait', 'kuwait.csv', '1'),
(5, 'Saudi Arabia', 'saudi_arabia.csv', '1'),
(6, 'Oman', 'oman.csv', '1');

-- --------------------------------------------------------

--
-- Table structure for table `csv_data`
--

CREATE TABLE `csv_data` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `phone_number` varchar(500) NOT NULL,
  `domain` varchar(500) NOT NULL,
  `compnay` varchar(500) NOT NULL,
  `city` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `registration_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `server` varchar(500) NOT NULL,
  `ip_address` varchar(500) NOT NULL,
  `user_id` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `category` varchar(500) NOT NULL,
  `source` varchar(500) NOT NULL,
  `status` varchar(500) NOT NULL,
  `interests` text NOT NULL,
  `isNew` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `csv_data`
--

INSERT INTO `csv_data` (`id`, `name`, `email`, `phone_number`, `domain`, `compnay`, `city`, `date`, `registration_date`, `expiry_date`, `server`, `ip_address`, `user_id`, `country`, `category`, `source`, `status`, `interests`, `isNew`) VALUES
(2, '', 'no-email@xyz.com', '123456', 'ankeoman.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', 'ns69.domaincontrol.com', '', '0', 'Oman', 'GoDaddy.com, LLC', '1', '1', 'asdf', 0),
(3, 'Abdulrahman Aljenaidel', 'aljenaidel@fonstele.com', '+971.42364545', 'aaljenaidel.com', '', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns4015.hostgator.com', '0', 'United Arab Emirates', 'Launchpad, Inc.', '2', '2', '', 0),
(4, 'Mojtaba Mirmajidi', 'mirmajidi@yahoo.com', '+1.2132672112', 'aket.net', '', 'Dubai', '2018-10-15', '2018-10-13', '2019-10-13', '', 'ns1.softek.ir', '0', 'United Arab Emirates', 'Name.com, Inc.', '3', '2', '', 0),
(5, 'OMAIR SAYA', 'receipts@dubicars.com', '+971.0563504151', 'alharujmotors.com', 'DUBICARS INTERNATIONAL FZ-LLC', 'DUBAI', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.bluehost.com', '0', 'United Arab Emirates', 'FastDomain Inc.', '3', '2', '', 0),
(6, 'Cherry Booc', 'messipilis@yahoo.com', '971502469737', 'angfilipino.com', 'billyboocarts', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', '3', '', '', 0),
(7, 'domain manager', 'admin@indiandomain.com', '+971.558995790', 'anywherecontent.com', '', 'dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'buy.internettraffic.com', '0', 'United Arab Emirates', 'DropCatch.com 496 LLC', '2', '', '', 0),
(8, 'TAFAWUQ FACILITIES MANAGEMENT LLC', 'bijuk@eltizam.ae', '+971.26980000', 'awktafawuq.com', 'TAFAWUQ FACILITIES MANAGEMENT LLC', 'ABU DHABI', '2018-10-15', '2018-10-14', '2020-10-14', '', 'auhans1.ecompany.ae', '0', 'United Arab Emirates', 'Emirates Telecommunications Corporation - Etisalat', '1', '', '', 0),
(9, 'Danielle Suchley', 'danielle.suchley@finsbury-associates.com', '+971.045674500', 'beeneple.com', 'Finsbury Associates', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(10, 'Web Villa', 'web_villa@hotmail.com', '+971.505185042', 'bestmateonline.com', 'Webvilla', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.bluehost.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(11, 'wassim mourad', 'abd.moussalli@adplus-sy.com', '+971.526783666', 'bet-sy.com', 'fmsi group', 'dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.aspnix.com', '0', 'United Arab Emirates', 'Name.com, Inc.', 'http://www.name.com', '', '', 0),
(12, 'Affira Liaquet', 'motibali6@gmail.com', '+971.543257057', 'bookingtourism.com', 'Affira Liaquet', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.hostinger.com', '0', 'United Arab Emirates', 'Hostinger, UAB', 'http://www.hostinger.com', '', '', 0),
(13, 'Mohamad Bin Dalmook', 'mohd.bindalmook@gmail.com', '+971.504817777', 'borrowisley.com', 'FOIL&FILM', 'DUBAI', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(14, 'Ivan Paul', 'lbcarpio09@gmail.com', '+971.26657502', 'digital-musketeer.com', '', 'Abu Dhabi', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns6425.hostgator.com', '0', 'United Arab Emirates', 'Launchpad, Inc.', 'http://www.launchpad.com', '', '', 0),
(15, 'Biz Infotech', 'sales@bizinfotechllc.com', '+971.556722405', 'dubaicarshipping.com', '', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.bizinfotechllc.com', '0', 'United Arab Emirates', 'PDR Ltd. d/b/a PublicDomainRegistry.com', 'http://www.publicdomainregistry.com', '', '', 0),
(16, 'Emirates Investment Fund for Development', 'emifd@spambog.com', '+971.52431100', 'emifd.com', '', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.hostblast.net', '0', 'United Arab Emirates', 'Namesilo, LLC', 'http://www.namesilo.com', '', '', 0),
(17, 'HAITHAM HELAL', 'haitham.dm1@gmail.com', '+971.568722764', 'googleadsclick.com', 'SPORT360', 'DUBAI', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.bluehost.com', '0', 'United Arab Emirates', 'FastDomain Inc.', 'http://www.fastdomain.com', '', '', 0),
(18, 'Imdad FZCO', 'groupimdad@gmail.com', '+971.503452715', 'imdad-i.info', 'Imdad FZCO', 'Dubai', '2018-10-15', '2018-10-15', '2019-10-15', '', 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(19, 'Frederik K?ie Jerner', 'fkj@g-n-i.com', '+971.555319409', 'karat14.com', 'Global Network International FZE', 'United Arab Emirates', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.alpnames.com', '0', 'United Arab Emirates', 'Alpnames Limited', 'http://www.alpnames.com', '', '', 0),
(20, 'MOHD SYOUFI', 'mohd@alsyoufi.com', '+971.509479955', 'kestrelexpress.com', 'TIJARAT', 'DUBAI', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(21, 'Ghafran Malik', 'malikghafran@gmail.com', '+971.0588084599', 'kimainteriors.com', 'Kima Interiors', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.hostinginuae.com', '0', 'United Arab Emirates', 'Buzinessware FZCO', '2', '', '', 0),
(22, 'Omkar Velapure', 'omvelapure@gmail.com', '+971.554678314', 'legendarymasketeers.com', '', 'Dubai', '2018-10-15', '2018-10-14', '2021-10-14', '', 'dns1.bigrock.in', '0', 'United Arab Emirates', 'BigRock Solutions Ltd.', '1', '', 'sadfsad', 0),
(23, '. -', '123@gmail.com', '+971.21234567', 'lokalcatering.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', '', 'auhans1.ecompany.ae', '0', 'United Arab Emirates', 'Emirates Telecommunications Corporation - Etisalat', 'http://www.nic.ae', '', '', 0),
(24, 'NITIN GOPINATH', 'lumiereemc.com@wix-domains.com', '+1.4159496022', 'lumiereemc.com', 'Lumiere', 'abudhabi', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns2.wixdns.net', '0', 'United Arab Emirates', 'Network Solutions, LLC', 'http://networksolutions.com', '', '', 0),
(25, 'yasser gamil', 'yasserg@disctech.net', '+971.506577965', 'luxxesfashion.com', 'PV Middle East', 'Sharjah', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns-us.1and1-dns.com', '0', 'United Arab Emirates', 'register.com, Inc.', 'http://www.register.com', '', '', 0),
(26, 'Rami Zaher', 'info@emkantech.com', '+971.507161262', 'motldp.com', 'Emkan Tech', 'Sharjah', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.emkantech.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(27, 'malak saifullah', 'malak_saifullah@hotmail.com', '+971.971503523917', 'mytwylah.com', '', 'dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.c02.tmdcloud.com', '0', 'United Arab Emirates', 'PDR Ltd. d/b/a PublicDomainRegistry.com', 'http://www.publicdomainregistry.com', '', '', 0),
(28, 'SEAN DAY -', 'seanaday@live.com', '+971.569556836', 'nasewingservice.com', '', 'ABU DHABI', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns10.wixdns.net', '0', 'United Arab Emirates', 'Emirates Telecommunications Corporation - Etisalat', 'http://www.nic.ae', '', '', 0),
(29, 'Abdeen Warsi', 'abdeenwarsi@yahoo.com', '+1.971554032724', 'northmanproducts.com', '', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'Name.com, Inc.', 'http://www.name.com', '', '', 0),
(30, 'yazid ntulume', 'mbowaali@gmail.com', '+971.524800683', 'nrscl.com', 'kapiki', 'Abu Dhabi', '2018-10-15', '2018-10-15', '2019-10-15', '', 'ns1.freehosting.com', '0', 'United Arab Emirates', 'PDR Ltd. d/b/a PublicDomainRegistry.com', 'http://www.publicdomainregistry.com', '', '', 0),
(31, 'Ripplette Corp.', 'it@bmb-group.com', '+971.48833940', 'nurofficial.com', 'Ripplette Corp', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(32, 'Naxish Ghaus', 'naxishghaus@gmail.com', '+971.971565258792', 'panthersmanagement.com', '', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns6383.hostgator.com', '0', 'United Arab Emirates', 'Launchpad, Inc.', 'http://www.launchpad.com', '', '', 0),
(33, 'Khuram Iqbal', 'khuram@800wisdom.ae', '+971.551543272', 'retailarms.com', 'Wisdom IT Solutions', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(34, 'Fajr Island Electronics L.L.C.', 'admin@fajri.om', '97143257047', 'samishoppingmall.com', 'Fajr Island Electronics L.L.C.', 'Dubai', '2018-10-15', '2018-10-15', '2019-10-15', '', 'dns1.supremepanel.com', '0', 'United Arab Emirates', 'Wild West Domains, LLC', 'http://www.wildwestdomains.com', '', '', 0),
(35, 'ahmed noureldeen', 'nour_eldeen_1@hotmail.com', '+971.569473305', 'skullgum.com', 'moka', 'dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1kpv.name.com', '0', 'United Arab Emirates', 'Name.com, Inc.', 'http://www.name.com', '', '', 0),
(36, 'forogh tavakolichaleshtary', 'rpds_co@yahoo.com', '+971.504620432', 'smartcityproces.com', 'parsonweb', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'mars1.vanda.host', '0', 'United Arab Emirates', 'Hosting Concepts B.V. dba Openprovider', 'http://www.openprovider.com', '', '', 0),
(37, 'United Communication Group - UCG', 'domains@ucg.ae', '+971.26393137', 'syriafriends.net', 'UCG L.L.C.', 'Abu Dhabi', '2018-10-15', '2018-10-13', '2019-10-13', '', 'ns1.scs-net.org', '0', 'United Arab Emirates', 'Name.com, Inc.', 'http://www.name.com', '', '', 0),
(38, 'Cenil Roy', 'cenilroy@dmgeventsme.com', '97144380355', 'thebig5constructqatar.com', 'dmg events', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(39, 'Hussein Kefel', 'contact@gulfnames.com', '971504691724', 'thegem.biz', 'Media', 'Dubai', '2018-10-15', '2018-10-11', '2019-10-11', '', 'ns1.eftydns.com', '0', 'United Arab Emirates', 'Dynadot, LLC', 'http://www.dynadot.com', '', '', 0),
(40, 'Tamanna Mishra', 'mishratamanna3@gmail.com', '+971.507892826', 'theshotter.com', 'START UP', 'Jebel Ali', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns6461.hostgator.com', '0', 'United Arab Emirates', 'Launchpad, Inc.', 'http://www.launchpad.com', '', '', 0),
(41, 'Thumbay Group', 'vignesh@thumbay.com', '+971.67431333', 'thumbayuniversityhospital.com', 'Thumbay Group', 'Ajman', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns25.worldnic.com', '0', 'United Arab Emirates', 'Network Solutions, LLC', 'http://networksolutions.com', '', '', 0),
(42, 'Akram Assaf', 'akram.assaf@gmail.com', '97144493100', 'usharek.com', 'Doctoruna', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(43, 'Alex Vargas', 'amv1121@yahoo.com', '+971.527552424', 'uzmarketplace.com', 'Soft Touch IT Infrastructure', 'Bur Dubai', '2018-10-15', '2018-10-14', '2020-10-14', '', 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(44, 'Aidan Adams', 'info@webgearmedia.com', '+971.555055688', 'venturesint.com', 'Webgear Media', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.cloudsector.net', '0', 'United Arab Emirates', 'Name.com, Inc.', 'http://www.name.com', '', '', 0),
(45, 'MAHMOUD HOSEINPOUR', 'maham1365@gmail.com', '+971.8491781111', 'vibovidabonsai.com', 'VIBO', 'DUBAI', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 0),
(46, 'PROJEX MIDDLE EAST', 'naseer132@gmail.com', '+971.0565323883', 'webtimez.com', '', 'DUBAI', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns1.bluehost.com', '0', 'United Arab Emirates', 'FastDomain Inc.', 'http://www.fastdomain.com', '', '', 0),
(47, 'SpotOn', 'marianne@spotonae.com', '+971.00971558461636', 'you-1st.com', 'SpotOn', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', '', 'ns97.worldnic.com', '0', 'United Arab Emirates', 'Network Solutions, LLC', 'http://networksolutions.com', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `engagement_types`
--

CREATE TABLE `engagement_types` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `engagement_types`
--

INSERT INTO `engagement_types` (`id`, `title`, `status`) VALUES
(1, 'Meeting', '1'),
(2, 'Call Later', '1');

-- --------------------------------------------------------

--
-- Table structure for table `files_uploaded`
--

CREATE TABLE `files_uploaded` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `path` varchar(1000) NOT NULL,
  `imported` enum('0','1') NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_import` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files_uploaded`
--

INSERT INTO `files_uploaded` (`id`, `title`, `path`, `imported`, `date_add`, `date_import`) VALUES
(1, 'Create Files Uploaded', 'uploads/Customers (1).csv', '0', '2018-10-30 09:16:18', '0000-00-00'),
(2, 'Create', 'uploads/Customers.csv', '0', '2018-10-30 09:19:34', '0000-00-00'),
(3, 'Create Files Uploaded', 'uploads/38682b1005dcc015e0c33dc9f70a2fc5.csv', '0', '2018-10-30 09:20:23', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `date_comment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `type` enum('main','sub') NOT NULL,
  `agenda` varchar(500) DEFAULT NULL,
  `date_time` datetime DEFAULT '0000-00-00 00:00:00',
  `venue` varchar(500) DEFAULT NULL,
  `interests` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `data_id`, `comments`, `date_comment`, `user_id`, `type`, `agenda`, `date_time`, `venue`, `interests`) VALUES
(1, 47, 'something', '2018-10-25 07:13:29', 0, 'main', 'Meeting', '2018-10-25 00:00:00', 'something', ''),
(2, 22, 'vfasdfsad', '2018-10-27 10:46:31', 0, 'main', '', '1970-01-01 01:00:00', '', ''),
(3, 22, 'sfsadf', '2018-10-27 10:47:06', 0, 'main', 'Call Later', '2018-10-17 00:00:00', 'sadfsdaf', ''),
(4, 2, 'fasdf', '2018-10-28 11:35:59', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'Testing', ''),
(5, 2, 'fasdf', '2018-10-28 11:35:59', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'Testing', ''),
(6, 2, 'fasdf', '2018-10-28 11:35:59', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'Testing', ''),
(7, 2, 'fasdf', '2018-10-28 11:35:59', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'Testing', ''),
(8, 2, 'Comments', '2018-10-28 11:38:56', 0, 'main', 'Meeting', '2018-10-17 00:00:00', '', ''),
(9, 2, 'fasdfasdf', '2018-10-28 11:39:50', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(10, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:56', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(11, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:57', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(12, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:57', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(13, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:57', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(14, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:57', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(15, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:57', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(16, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:58', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(17, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:58', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(18, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:58', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(19, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:58', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(20, 2, 'fasdfasdf asdfasdf', '2018-10-28 11:39:58', 0, 'main', 'Meeting', '2018-10-28 00:00:00', 'asdf', ''),
(21, 2, 'comment with testug', '2018-10-28 11:42:50', 0, 'main', 'Meeting', '2018-10-17 00:30:00', 'JLT, Dubai UAE', ''),
(22, 2, 'asdfas', '2018-10-28 11:53:25', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'Testing', ''),
(23, 2, 'sdfaasd', '2018-10-28 11:54:58', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'asdfsdaf', ''),
(24, 2, 'Testing', '2018-10-28 12:16:39', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(25, 2, 'Testing', '2018-10-28 12:16:48', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(26, 2, 'Testing', '2018-10-28 12:16:49', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(27, 2, 'Testing', '2018-10-28 12:16:49', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(28, 2, 'Testing', '2018-10-28 12:16:49', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(29, 2, 'Testing', '2018-10-28 12:16:49', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(30, 2, 'Testing', '2018-10-28 12:16:49', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(31, 2, 'Testing', '2018-10-28 12:16:50', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(32, 2, 'Testing', '2018-10-28 12:16:50', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(33, 2, 'Testing', '2018-10-28 12:16:50', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(34, 2, 'prepend', '2018-10-28 12:17:49', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(35, 2, 'Testingsafasdf', '2018-10-28 12:18:11', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(36, 2, 'Testingsafasdf', '2018-10-28 12:18:13', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(37, 2, 'Testingsafasdf', '2018-10-28 12:18:14', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(38, 2, 'Testingsafasdf', '2018-10-28 12:18:14', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(39, 2, 'Testingsafasdf', '2018-10-28 12:18:14', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(40, 2, 'Testingsafasdf', '2018-10-28 12:18:14', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(41, 2, 'Testingsafasdf', '2018-10-28 12:18:14', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(42, 2, 'Testingsafasdf', '2018-10-28 12:18:15', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(43, 2, 'Testingsafasdf', '2018-10-28 12:18:15', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(44, 2, 'Testingsafasdf', '2018-10-28 12:18:15', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(45, 2, 'Testingsafasdf', '2018-10-28 12:18:15', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(46, 2, 'Testingsafasdf', '2018-10-28 12:18:15', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(47, 2, 'Testingsafasdf', '2018-10-28 12:18:15', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(48, 2, 'Testingsafasdf', '2018-10-28 12:18:16', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'prepend', ''),
(49, 2, 'Testingsafasdf', '2018-10-28 12:18:19', 0, 'main', 'Call Later', '1970-01-01 01:00:00', 'prepend', ''),
(50, 2, 'Prepend', '2018-10-28 12:19:11', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'asdfasdf', ''),
(51, 2, 'This is log saved', '2018-10-28 12:19:25', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'asdfasdf', ''),
(52, 2, 'This is log saved', '2018-10-28 12:22:47', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'asdfasdf', ''),
(53, 20, 'Testing Comments ', '2018-10-28 12:32:07', 0, 'main', 'Call Later', '2018-10-17 00:30:00', 'JLT, Dubai UAE', ''),
(54, 20, 'Testing Comments ', '2018-10-28 12:32:13', 0, 'main', 'Call Later', '2018-10-17 00:30:00', 'JLT, Dubai UAE', ''),
(55, 20, '3. Testing Comments ', '2018-10-28 12:32:20', 0, 'main', 'Call Later', '2018-10-17 00:30:00', 'JLT, Dubai UAE', ''),
(56, 20, '3. Testing Comments ', '2018-10-28 12:32:39', 0, 'main', 'Call Later', '2018-10-17 00:30:00', 'JLT, Dubai UAE', ''),
(57, 21, 'Testi', '2018-10-28 12:33:05', 0, 'main', 'Meeting', '2018-10-17 00:30:00', 'JLT, Dubai UAE', ''),
(58, 2, 'this is awesome ', '2018-10-28 12:47:52', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(59, 2, '12312312312313cthis is awesome ', '2018-10-28 12:48:03', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(60, 41, 'Logs testing', '2018-10-28 13:27:34', 0, 'main', 'Meeting', '2018-10-17 00:00:00', 'JLT, Dubai UAE', ''),
(61, 2, 'fsadf', '2018-10-29 08:54:36', 0, 'main', 'Meeting', '2018-10-29 06:55:00', 'Testing', ''),
(62, 2, 'jkwkjfhasjkhssaf', '2018-10-29 09:02:28', 0, 'main', 'Meeting', '2018-10-29 13:05:00', 'JLT, Dubai UAE', ''),
(63, 2, 'sdaf', '2018-10-29 09:21:23', 0, 'main', 'Meeting', '1899-12-31 00:00:00', 'asdfasdf', 'asdfasdf'),
(64, 2, 'fasdf', '2018-10-29 09:21:49', 0, 'main', 'Meeting', '1899-12-14 14:50:00', 'JLT, Dubai UAE', 'asdfsadf'),
(65, 2, 'fasdf', '2018-10-29 09:22:10', 0, 'main', 'Meeting', '1899-12-14 14:50:00', 'JLT, Dubai UAE', 'asdfsadf,fsadfasdf,asdf'),
(66, 2, 'fasdf', '2018-10-29 09:23:04', 0, 'main', 'Meeting', '1899-12-14 14:50:00', 'JLT, Dubai UAE', 'asdfsadf,fsadfasdf,asdf'),
(67, 2, 'Comments', '2018-10-29 09:33:06', 0, 'main', 'Meeting', '2018-10-29 13:20:00', 'JLT, Dubai UAE', 'interests,call'),
(68, 2, 'logs', '2018-10-29 09:45:27', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'dsfg', 'logs,save,update,delete,reject,status,joining,Disinfection,affection,amicable'),
(69, 2, 'logs', '2018-10-29 09:46:17', 0, 'main', 'Meeting', '1970-01-01 01:00:00', 'dsfg', 'logs,save,update,delete,reject,status,joining,Disinfection,affection,amicable'),
(70, 1, 'Logs', '2018-10-29 10:11:40', 0, 'sub', 'Meeting', '2018-10-30 17:45:00', 'JLT, Dubai UAE', 'Intersest'),
(71, 1, 'Logs', '2018-10-29 10:14:11', 0, 'sub', 'Meeting', '2018-10-30 17:45:00', 'JLT, Dubai UAE', 'Intersest');

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE `sources` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `title`, `status`) VALUES
(1, 'Website', '1'),
(2, 'web to Lead', '1'),
(3, 'Social Media', '1');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `active` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `title`, `active`) VALUES
(1, 'Created', '1'),
(2, 'On hold', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sub_data`
--

CREATE TABLE `sub_data` (
  `id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `phone_number` varchar(500) DEFAULT NULL,
  `domain` varchar(500) DEFAULT NULL,
  `compnay` varchar(500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `server` varchar(500) DEFAULT NULL,
  `ip_address` varchar(500) DEFAULT NULL,
  `user_id` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `category` varchar(500) DEFAULT NULL,
  `source` varchar(500) NOT NULL,
  `status` varchar(500) NOT NULL,
  `interests` text NOT NULL,
  `isNew` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_data`
--

INSERT INTO `sub_data` (`id`, `data_id`, `name`, `email`, `phone_number`, `domain`, `compnay`, `city`, `date`, `registration_date`, `expiry_date`, `server`, `ip_address`, `user_id`, `country`, `category`, `source`, `status`, `interests`, `isNew`) VALUES
(1, 2, '', 'no-email@xyz.com', '123456', 'delmonmuscat.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', 'ns69.domaincontrol.com', NULL, '0', 'Oman', 'GoDaddy.com, LLC', '2', '2', '', 1),
(2, 2, '', 'no-email@xyz.com', '123456', 'dermaclinica.net', '', '', '2018-10-15', '2018-10-14', '2020-10-14', 'ns69.domaincontrol.com', NULL, '0', 'Oman', 'GoDaddy.com, LLC', '1', '', '', 1),
(3, 2, 'REDACTED FOR PRIVACY', 'no-email@xyz.com', 'REDACTED FOR PRIVACY', 'fanar.company', 'fnaralnsr', 'REDACTED FOR PRIVACY', '2018-10-15', '2018-10-14', '2019-10-14', '', NULL, '0', 'Oman', 'CSL Computer Service Langenbach GmbH d/b/a joker.com', '3', '', '', 1),
(4, 2, '', 'no-email@xyz.com', '123456', 'mataem.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', 'ns69.domaincontrol.com', NULL, '0', 'Oman', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(5, 2, '', 'no-email@xyz.com', '123456', 'offyzone.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', 'ns69.domaincontrol.com', NULL, '0', 'Oman', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(6, 2, '', 'no-email@xyz.com', '123456', 'sjfoman.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', 'ns31.domaincontrol.com', NULL, '0', 'Oman', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(7, 1, 'registrant_name', 'registrant_email', 'registrant_phone', 'domain_name', 'registrant_company', 'registrant_city', '1970-01-01', '1970-01-01', '1970-01-01', NULL, 'name_server_1', '0', 'registrant_country', 'domain_registrar_name', 'domain_registrar_url', '', '', 1),
(8, 2, '', 'no-email@xyz.com', '123456', 'accstrdg.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(9, 2, '', 'no-email@xyz.com', '123456', 'adamallysdxb.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(10, 2, '', 'no-email@xyz.com', '123456', 'aerodunellc.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(11, 2, '', 'no-email@xyz.com', '123456', 'alltixs.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(12, 2, 'Shell Bird', 'no-email@xyz.com', '123456', 'allweconnect.com', 'Shell Bird', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(13, 2, 'Shell Bird', 'no-email@xyz.com', '123456', 'allyouconnect.com', 'Shell Bird', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(14, 2, '', 'no-email@xyz.com', '123456', 'alramlahsafety.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(15, 2, '', 'no-email@xyz.com', '123456', 'anaqeeduae.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(16, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'appviewers.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(17, 2, 'REDACTED FOR PRIVACY', 'no-email@xyz.com', 'REDACTED FOR PRIVACY', 'arango.international', '', 'REDACTED FOR PRIVACY', '2018-10-15', '2018-10-14', '2020-10-14', NULL, '', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(18, 2, '', 'no-email@xyz.com', '123456', 'autocashdeal.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(19, 2, '', 'no-email@xyz.com', '123456', 'autocashdeal.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(20, 2, '', 'no-email@xyz.com', '123456', 'bct-trading.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(21, 2, 'Wise Point Consultancy', 'no-email@xyz.com', '123456', 'beingcookuae.com', 'Wise Point Consultancy', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(22, 9, 'Danielle Suchley', 'danielle.suchley@finsbury-associates.com', '+971.045674500', 'beneple.net', 'Finsbury Associates', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(23, 9, 'Danielle Suchley', 'danielle.suchley@finsbury-associates.com', '+971.045674500', 'beneple.online', 'Finsbury Associates', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(24, 11, 'wassim mourad', 'abd.moussalli@adplus-sy.com', '+971.526783666', 'bet-sy.net', 'fmsi group', 'dubai', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns1.aspnix.com', '0', 'United Arab Emirates', 'Name.com, Inc.', 'http://www.name.com', '', '', 1),
(25, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'beyondclinical.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(26, 2, '', 'no-email@xyz.com', '123456', 'burraqtechnical.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(27, 2, '', 'no-email@xyz.com', '123456', 'call4bestcom.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(28, 2, 'ACORUSPHARMA', 'no-email@xyz.com', '123456', 'caremyears.com', 'ACORUSPHARMA', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(29, 10, 'Web Villa', 'web_villa@hotmail.com', '+971.505185042', 'cec-equip-uk.com', 'Webvilla', 'Dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(30, 2, '', 'no-email@xyz.com', '123456', 'choithrams-group.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(31, 2, '', 'no-email@xyz.com', '123456', 'choithramsglobal.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(32, 2, '', 'no-email@xyz.com', '123456', 'choithramshq.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(33, 2, '', 'no-email@xyz.com', '123456', 'choithramsint.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(34, 2, '', 'no-email@xyz.com', '123456', 'choithramsllc.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(35, 2, '', 'no-email@xyz.com', '123456', 'choithramsoffice.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(36, 2, '', 'no-email@xyz.com', '123456', 'choithramsorg.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(37, 2, '', 'no-email@xyz.com', '123456', 'choithramsworldwide.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(38, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'coachcircuit.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(39, 2, '', 'no-email@xyz.com', '123456', 'colorcloudme.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(40, 2, 'Shell Bird', 'no-email@xyz.com', '123456', 'connectyouall.com', 'Shell Bird', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(41, 2, 'Cotton House General Trading', 'no-email@xyz.com', '123456', 'cotthouse.com', 'Cotton House General Trading', '', '2018-10-15', '2018-10-14', '2028-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(42, 7, 'domain manager', 'admin@indiandomain.com', '+971.558995790', 'crewpot.com', '', 'dubai', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'DROPCATCH.COM 806 LLC', 'http://www.dropcatch806.com', '', '', 1),
(43, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'dietsherpa.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(44, 2, '', 'no-email@xyz.com', '123456', 'dronlineusa.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(45, 2, '', 'no-email@xyz.com', '123456', 'droppdfashion.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(46, 2, '', 'no-email@xyz.com', '123456', 'dubaiafricainvestments.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(47, 2, '', 'no-email@xyz.com', '123456', 'dubaifurnitureworld.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(48, 2, '', 'no-email@xyz.com', '123456', 'dubaiworldfurniture.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(49, 2, '', 'no-email@xyz.com', '123456', 'ecowebsites2.net', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(50, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'enhancedformulas.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(51, 2, '', 'no-email@xyz.com', '123456', 'erppundit.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(52, 2, '', 'no-email@xyz.com', '123456', 'expo2055.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(53, 2, '', 'no-email@xyz.com', '123456', 'findmytuitor.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(54, 9, 'Danielle Suchley', 'danielle.suchley@finsbury-associates.com', '+971.045674500', 'finsburyassociate.com', 'Finsbury Associates', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(55, 2, '', 'no-email@xyz.com', '123456', 'fleetmama.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(56, 2, '', 'no-email@xyz.com', '123456', 'flyyoza.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(57, 2, '', 'no-email@xyz.com', '123456', 'furnitureworlddubai.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(58, 2, '', 'no-email@xyz.com', '123456', 'future1redcarpet.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(59, 2, '', 'no-email@xyz.com', '123456', 'gaetrading.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(60, 2, '', 'no-email@xyz.com', '123456', 'globaltranscript.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(61, 2, '', 'no-email@xyz.com', '123456', 'glutenfighter.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(62, 2, '', 'no-email@xyz.com', '123456', 'idxperts.net', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(63, 2, '', 'no-email@xyz.com', '123456', 'intrayoga.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(64, 2, '', 'no-email@xyz.com', '123456', 'jetintegrate.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(65, 2, '', 'no-email@xyz.com', '123456', 'jobsinminutes.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(66, 2, 'YMR Trading LLC', 'no-email@xyz.com', '123456', 'kanburry.com', 'YMR Trading LLC', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'austin.ns.cloudflare.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(67, 19, 'Frederik K?ie Jerner', 'fkj@g-n-i.com', '+971.555319409', 'karat18.com', 'Global Network International FZE', 'United Arab Emirates', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns1.alpnames.com', '0', 'United Arab Emirates', 'Alpnames Limited', 'http://www.alpnames.com', '', '', 1),
(68, 2, '', 'no-email@xyz.com', '123456', 'karinadiana.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(69, 2, '', 'no-email@xyz.com', '123456', 'khalifaventures.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(70, 2, '', 'no-email@xyz.com', '123456', 'krishnaprintersandexports.com', '', '', '2018-10-15', '2018-10-14', '2023-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(71, 2, '', 'no-email@xyz.com', '123456', 'lafashionrazzi.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(72, 2, '', 'no-email@xyz.com', '123456', 'ldrlab.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(73, 2, '', 'no-email@xyz.com', '123456', 'luxuryhouseevents.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns1.site5.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(74, 2, '', 'no-email@xyz.com', '123456', 'maneandmane.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(75, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'marblepaver.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(76, 2, '', 'no-email@xyz.com', '123456', 'maxi-kart.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns11.alipartnership.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(77, 2, '', 'no-email@xyz.com', '123456', 'mazayagov.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(78, 2, '', 'no-email@xyz.com', '123456', 'mazayagovservice.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(79, 2, '', 'no-email@xyz.com', '123456', 'medgovern.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(80, 2, '', 'no-email@xyz.com', '123456', 'mhmrserver.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(81, 2, 'REDACTED FOR PRIVACY', 'no-email@xyz.com', 'REDACTED FOR PRIVACY', 'mobodl.com', 'REDACTED FOR PRIVACY', 'REDACTED FOR PRIVACY', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns1.hanista.ir', '0', 'United Arab Emirates', '1API GmbH', 'http://www.1api.net', '', '', 1),
(82, 2, '', 'no-email@xyz.com', '123456', 'modemos.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(83, 2, '', 'no-email@xyz.com', '123456', 'music360alive.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(84, 2, 'REDACTED FOR PRIVACY', 'no-email@xyz.com', 'REDACTED FOR PRIVACY', 'myflavours.restaurant', '', 'REDACTED FOR PRIVACY', '2018-10-15', '2018-10-14', '2019-10-14', NULL, '', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(85, 2, '', 'no-email@xyz.com', '123456', 'myflavoursdubai.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(86, 2, '', 'no-email@xyz.com', '123456', 'myflavoursrestaurant.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(87, 2, '', 'no-email@xyz.com', '123456', 'myfutureconsultancy.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(88, 2, '', 'no-email@xyz.com', '123456', 'mytinygig.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(89, 2, '', 'no-email@xyz.com', '123456', 'naazcosmetics.com', '', '', '2018-10-15', '2018-10-15', '2020-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(90, 2, '', 'no-email@xyz.com', '123456', 'nazzcosmetics.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(91, 31, 'Ripplette Corp.', 'it@bmb-group.com', '+971.48833940', 'nurofficiel.com', 'Ripplette Corp', 'Dubai', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(92, 2, '', 'no-email@xyz.com', '123456', 'ohjevents.com', '', '', '2018-10-15', '2018-10-14', '2021-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(93, 2, '', 'no-email@xyz.com', '123456', 'omarsalameh.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(94, 2, '', 'no-email@xyz.com', '123456', 'onceuponahotel.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(95, 2, '', 'no-email@xyz.com', '123456', 'papers2program.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(96, 2, '', 'no-email@xyz.com', '123456', 'pepol.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(97, 2, '', 'no-email@xyz.com', '123456', 'petsoman.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(98, 2, '', 'no-email@xyz.com', '123456', 'plainlook.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(99, 2, '', 'no-email@xyz.com', '123456', 'planeventor.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(100, 2, '', 'no-email@xyz.com', '123456', 'pplconsult.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(101, 2, '', 'no-email@xyz.com', '123456', 'profitvisions.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(102, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'propshout.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(103, 2, 'Qaraz Deals', 'no-email@xyz.com', '123456', 'qdinspire.com', 'Qaraz Deals', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(104, 2, '', 'no-email@xyz.com', '123456', 'queenofbookings.com', '', '', '2018-10-15', '2018-10-14', '2021-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(105, 2, '', 'no-email@xyz.com', '123456', 'rainlabdesign.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(106, 2, '', 'no-email@xyz.com', '123456', 'ramctrading.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(107, 2, '', 'no-email@xyz.com', '123456', 'raninvestments.com', '', '', '2018-10-15', '2018-10-14', '2021-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(108, 2, '', 'no-email@xyz.com', '123456', 'rayadi.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns1.tqniyati.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(109, 2, '', 'no-email@xyz.com', '123456', 'raymondint.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(110, 2, '', 'no-email@xyz.com', '123456', 'raymondinternationalgcc.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(111, 2, '', 'no-email@xyz.com', '123456', 'remote4gate.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(112, 2, '', 'no-email@xyz.com', '123456', 'riceandshineagriculturaltrading.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(113, 2, '', 'no-email@xyz.com', '123456', 'rightonsport.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(114, 2, '', 'no-email@xyz.com', '123456', 'robustgroups.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'cns509.hostgator.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(115, 2, '', 'no-email@xyz.com', '123456', 'royalprintdubai.online', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(116, 2, '', 'no-email@xyz.com', '123456', 'saaglem.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns1071.ifastnet.com', '0', 'United Arab Emirates', 'NameCheap, Inc.', 'http://www.namecheap.com', '', '', 1),
(117, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'saloninsight.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(118, 2, '', 'no-email@xyz.com', '123456', 'samgroupae.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(119, 2, '', 'no-email@xyz.com', '123456', 'saudicomm.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(120, 2, '', 'no-email@xyz.com', '123456', 'seancouture.com', '', '', '2018-10-15', '2018-10-14', '2021-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(121, 2, '', 'no-email@xyz.com', '123456', 'senior-jackets.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(122, 2, '', 'no-email@xyz.com', '123456', 'shapeomatic.com', '', '', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(123, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'shelfpal.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(124, 2, '', 'no-email@xyz.com', '123456', 'sidratransport.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(125, 2, '', 'no-email@xyz.com', '123456', 'silver-shadow.net', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(126, 2, '', 'no-email@xyz.com', '123456', 'sistersbeyondborders.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(127, 2, '', 'no-email@xyz.com', '123456', 'someindia.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(128, 2, '', 'no-email@xyz.com', '123456', 'souqwaniya.com', '', '', '2018-10-15', '2018-10-14', '2021-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(129, 2, '', 'no-email@xyz.com', '123456', 'starshellgroup.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(130, 2, '', 'no-email@xyz.com', '123456', 'stocktakelive.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(131, 2, '', 'no-email@xyz.com', '123456', 'telecaredynamics.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(132, 2, '', 'no-email@xyz.com', '123456', 'telemart.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(133, 2, '', 'no-email@xyz.com', '123456', 'telemartcare.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(134, 2, '', 'no-email@xyz.com', '123456', 'thawfeeqtradingdxb.info', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(135, 2, '', 'no-email@xyz.com', '123456', 'thecontainedmess.com', '', '', '2018-10-15', '2018-10-15', '2020-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(136, 2, '', 'no-email@xyz.com', '123456', 'thecontainedmess.info', '', '', '2018-10-15', '2018-10-15', '2020-10-15', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(137, 2, '', 'no-email@xyz.com', '123456', 'thecrescenttech.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(138, 2, '', 'no-email@xyz.com', '123456', 'thehealthiermenow.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(139, 2, 'Themaar Investment', 'no-email@xyz.com', '123456', 'themaarinvest.com', 'Themaar Investment', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(140, 2, '', 'no-email@xyz.com', '123456', 'tix4me.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(141, 2, '', 'no-email@xyz.com', '123456', 'ugabay.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(142, 2, '', 'no-email@xyz.com', '123456', 'usmanabbasi.com', '', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(143, 43, 'Alex Vargas', 'amv1121@yahoo.com', '+971.527552424', 'uzshopping.com', 'Soft Touch IT Infrastructure', 'Bur Dubai', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(144, 43, 'Alex Vargas', 'amv1121@yahoo.com', '+971.527552424', 'vargasfarm.com', 'Soft Touch IT Infrastructure', 'Bur Dubai', '2018-10-15', '2018-10-14', '2023-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(145, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'videopatient.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(146, 7, 'ahura fze', 'admin@indiandomain.com', '97147094224', 'viewerinteraction.com', '', 'dubai', '2018-10-15', '2018-10-15', '2019-10-15', NULL, 'buy.internettraffic.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(147, 2, '', 'no-email@xyz.com', '123456', 'visarundxb.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(148, 2, 'Management Partners Consultants', 'no-email@xyz.com', '123456', 'vr-dom.com', 'Management Partners Consultants', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(149, 2, 'Management Partners Consultants', 'no-email@xyz.com', '123456', 'vr-domes.com', 'Management Partners Consultants', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(150, 2, 'Management Partners Consultants', 'no-email@xyz.com', '123456', 'vr-iq.com', 'Management Partners Consultants', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(151, 2, 'Management Partners Consultants', 'no-email@xyz.com', '123456', 'vr-z1.com', 'Management Partners Consultants', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(152, 2, 'Management Partners Consultants', 'no-email@xyz.com', '123456', 'vr-zones.com', 'Management Partners Consultants', '', '2018-10-15', '2018-10-14', '2020-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(153, 2, '', 'no-email@xyz.com', '123456', 'wearemusic360.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(154, 2, '', 'no-email@xyz.com', '123456', 'wellbooth.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns31.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(155, 2, '', 'no-email@xyz.com', '123456', 'wwpdubai.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(156, 2, 'Shell Bird', 'no-email@xyz.com', '123456', 'youconnectall.com', 'Shell Bird', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1),
(157, 2, '', 'no-email@xyz.com', '123456', 'ytk138.com', '', '', '2018-10-15', '2018-10-14', '2019-10-14', NULL, 'ns69.domaincontrol.com', '0', 'United Arab Emirates', 'GoDaddy.com, LLC', 'http://registrar.godaddy.com', '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `csv_data`
--
ALTER TABLE `csv_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `engagement_types`
--
ALTER TABLE `engagement_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files_uploaded`
--
ALTER TABLE `files_uploaded`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_data`
--
ALTER TABLE `sub_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `csv_data`
--
ALTER TABLE `csv_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `engagement_types`
--
ALTER TABLE `engagement_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `files_uploaded`
--
ALTER TABLE `files_uploaded`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sub_data`
--
ALTER TABLE `sub_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
