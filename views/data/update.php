<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CsvData */

$this->title = 'Update Csv Data: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Csv Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="csv-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
