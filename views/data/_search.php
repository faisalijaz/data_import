<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CsvDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="csv-data-search background_card">

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="row">

            <div class="col-md-3">
                <?= $form->field($model, 'name') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'email') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'phone_number') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'domain') ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-3">
                <?php echo $form->field($model, 'compnay') ?>
            </div>

            <div class="col-md-3">
                <?php echo $form->field($model, 'country')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\app\models\Countries::find()->all(),'name','name'),
                        ['prompt' => 'Select ... ']
                ); ?>
            </div>

            <div class="col-md-3">
                <?php echo $form->field($model, 'category') ?>
            </div>

            <div class="col-md-3">
                <?php echo $form->field($model, 'source')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\app\models\Sources::find()->all(), 'id', 'title')
                    , ['prompt' => 'Select...']); ?>
            </div>
        </div>


        <div class="row">

            <div class="col-md-3">
                <?php echo $form->field($model, 'status')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\app\models\Status::find()->all(), 'title', 'title')
                    , ['prompt' => 'Select...']); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->field($model, 'isNew')->dropDownList(
                    ['1' => 'New', '0' => 'Old']
                    , ['prompt' => 'Select...']); ?>
            </div>

        </div>



        <div class="col-md-3 pull-right text-right">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
