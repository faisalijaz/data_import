<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Sources;
use yii\helpers\Url;
use app\models\Status;
/* @var $this yii\web\View */
/* @var $model app\models\CsvData */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Csv Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="csv-data-view background_card">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="card">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                'email:email',
                'phone_number',
                'domain',
                'compnay',
                'city',
                'date',
                'registration_date',
                'expiry_date',
                'server',
                'ip_address',
                'country',
                'category',
            ],
        ]); ?>
    </div>


    <?= \yii\grid\GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $model->getSubData(),
            'pagination' => [
                'pageSize' => 10,
            ]
        ]),
        'columns' => [

            'domain',
            'compnay',
            'city',
            'country',
            [
                'attribute' => 'interests',
                'format' => 'raw',
                'value' => function ($model) {

                    $interests = explode(',', $model->interests);

                    $html = "";
                    if(count($interests) > 0) {
                        foreach ($interests as $interest) {
                            $html .= "<span class='label label-info'>" . $interest . "</span> ";
                        }
                    }
                    return $html;
                }
            ],
            [
                'attribute' => 'source',
                'format' => 'raw',
                'value' => function ($model) {

                    $sources = Sources::find()->where(['id' => $model->source])->one();
                    $_source = "";
                    if ($sources <> null) {
                        $_source = $sources->title;
                    }

                    return '<span id="source_' . $model->id . '">' . $_source . '</span>' .
                        Html::a('<i class="fa fa-pencil"></i>', '#', [
                            'class' => 'showModalButton',
                            'value' => Url::to(['/data/change-source', 'id' => $model->id, 'type' => 'sub']),
                            'title' => Yii::t('yii', 'Change Source'),
                            'style' => 'padding:5px;'
                        ]);

                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {

                    $status = Status::find()->where(['id' => $model->source])->one();
                    $_status = "";

                    if ($status <> null) {
                        $_status = $status->title;
                    }

                    return '<span id="status_' . $model->id . '">' . $_status . '</span>' .
                        Html::a('<i class="fa fa-pencil"></i>', '#', [
                            'class' => 'showModalButton',
                            'value' => Url::to(['/data/change-status', 'id' => $model->id, 'type' => 'main']),
                            'title' => Yii::t('yii', 'Change Source'),
                            'style' => 'padding:5px;'
                        ]);
                }
            ],
            'country',
            'category',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}{logs}',
                'buttons' => [
                    'logs' => function ($url, $model) {
                        return Html::a('<span class="label label-info"><span class="glyphicon glyphicon-comment"></span></span>', '#', [
                            'class' => 'showModalButton',
                            'value' => Url::to(['/data/save-logs', 'id' => $model->id, 'type' => 'sub']),
                            'title' => Yii::t('yii', 'Logs'),
                            'id' => 'commentLog-' . $model->id
                        ]);
                    },

                ],

            ],
        ],
    ]); ?>

</div>



<?= $this->registerJs('
    
    $("body").on("click", ".sub_change_status", function(e) {
     
        e.preventDefault(); 
        var data_id = $(this).attr("id"); 
        var status = $(this).val();
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/data/update-status/",
            type: "POST",
            data: { status: status , id : data_id, type : "main"}, 
            success : function(res){
                
                console.log(res);
                
                var text = "UnApproved"; 
                
                if(res == "1"){ 
                     
                    swal({
                        title: "Success",
                        text: "Member " + text,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }     
                 
                $(this).attr("disabled",false);
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
    $("body").on("click", ".sub_change_status", function(e) {
     
        e.preventDefault(); 
        var data_id = $(this).attr("id"); 
        var status = $(this).val();
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/data/update-status/",
            type: "POST",
            data: { status: status , id : data_id , type : "sub" }, 
            success : function(res){
                
                console.log(res);
                
                var text = "UnApproved"; 
                
                if(res == "1"){ 
                     
                    swal({
                        title: "Success",
                        text: "Member " + text,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }     
                 
                $(this).attr("disabled",false);
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
    
    $("body").on("click", ".sub_change_source", function(e) {
     
        e.preventDefault(); 
        var data_id = $(this).attr("id"); 
        var source = $(this).val();
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/data/update-source-sub/",
            type: "POST",
            data: { source: source , id : data_id}, 
            success : function(res){
                
                if(res){
                    $("#success").removeClass("hidden");
                    $("#source_"+ data_id).text($(".change_source option:selected").text());
                }else{
                    $("#error").removeClass("hidden");
                }
               
               setTimeout(function(){ 
                    $(".change_source_label").addClass("hidden"); 
                    $("#modal").hide();
               }, 2000);
            },
            error : function(data){
                $("#error").removeClass("hidden");
                setTimeout(function(){ $(".change_source_label").addClass("hidden"); }, 2000);
            }
        }); 
    });
    
    
    $("body").on("click", "#SubmitForms", function(e) {
       
        var formAction = $("#LogsForm").attr("action");
        $.ajax({
           type: "POST",
           url: formAction,
           data: $("#LogsForm").serialize(),
           success: function (data) { 
               $("body").find("#comments_logs_container").html(data);
           },
           error: function () {
                alert("Error!!!");
           }
        });
        
        e.preventDefault();
    });
    
'); ?>