<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CsvData */
/* @var $form yii\widgets\ActiveForm */
?>
<!---->

<div class="csv-data-form">
    <div class="row">

        <div class="col-md-12">
            <?php
            echo Html::dropDownList('source',
                $model->source,
                ArrayHelper::map(\app\models\Sources::find()->all(), 'id', 'title'), [
                    'id' => $model->id,
                    'class' => 'change_source sub_change_source form-control',
                    'prompt' => 'Select ...'
                ]);
            ?>
        </div>
        <div class="col-md-12">
            <div class="label label-success change_source_label hidden" id="success">Source changed!</div>
            <div class="label label-success change_source_label hidden" id="error">Error! Source could not be changed!
            </div>
        </div>
        <div class="col-md-12"  style="position: absolute;bottom: 25px;text-align: right;right: 10px;">
            <span class="button btn btn-primary" data-dismiss="modal" aria-label="Close">close</span>
        </div>
    </div>
</div>
