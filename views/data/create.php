<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CsvData */

$this->title = 'Create Csv Data';
$this->params['breadcrumbs'][] = ['label' => 'Csv Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="csv-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
