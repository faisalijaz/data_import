<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Merge Duplicates';
?>
<div class="site-index">
    <div class="jumbotron">
        <h4>Attention!</h4>
        <p class="lead">
            You have <?= count(\Yii::$app->session->get('duplicates'));?> duplicate records in the list. Do you wish to merge this?
        </p>
        
        <p>
            <?= Html::a('Yes! Merge', Url::to(['/data/save-duplicates']), ['data-method' => 'POST', 'class' => 'btn btn-lg btn-success']) ?>
            <a class="btn btn-lg btn-warning" href="/data">No! go to listing</a>
        </p>
    </div>
    <div class="body-content"><div class="row"></div></div>
</div>