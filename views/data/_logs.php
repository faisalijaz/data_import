<?php

use app\models\EngagementTypes;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CsvData */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="csv-data-form">
    <div class="row">

        <div class="col-md-5">
            <?php $form = ActiveForm::begin(['action' => ['/data/submit-logs', 'id' => $id, 'type' => $type], 'options' => [
                'data-pjax' => true,
                'id' => 'LogsForm',
            ]]); ?>

            <div class="col-md-12 white_bg">

                <div class="form-group">
                    <?= $form->field($model, 'comments')->textarea(['class' => 'form-control']) ?>
                    <?= $form->field($model, 'type')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'data_id')->hiddenInput()->label(false); ?>
                </div>

                <div class="form-group">
                    <?= $form->field($model, 'interests')->widget(\wbraganca\tagsinput\TagsinputWidget::classname(), [
                        'clientOptions' => [
                            'trimValue' => true,
                            'allowDuplicates' => false
                        ]
                    ]); ?>

               </div>
                <p>
                    <strong>What Next?</strong>
                </p>
                <div class="form-group">
                    <?= $form->field($model, 'agenda')->dropDownList(
                        ArrayHelper::map(EngagementTypes::find()->all(), 'title', 'title'),
                        ['class' => 'form-control', 'prompt' => 'Select.. ']);
                    ?>
                </div>

                <div class="row">
                    <div class="col-md-12 form-group">
                        <?= $form->field($model, 'date_time')->widget(\kartik\widgets\DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Enter date ...'],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'type' => \kartik\date\DatePicker::TYPE_RANGE,
                                'todayHighlight' => true
                            ]
                        ]); ?>
                    </div>


                </div>

                <div class="form-group">
                    <?= $form->field($model, 'venue')->textInput(['class' => 'form-control']) ?>
                </div>
                <div class="col-md-12" style="position: absolute;bottom: 0px;text-align: right;right: 10px;">
                    <span class="button btn btn-primary" data-dismiss="modal" aria-label="Close"
                          style="margin-right: 10px;">close</span>
                    <?= Html::button('Save', ['id' => 'SubmitForms', 'class' => 'btn btn-success pull-right']) ?>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>

        <div class="col-md-7">
            <div class="box box-widget">
                <div class="box-footer box-comments">
                    <div id="comments_logs_container">
                        <?php
                        if (isset($logs)) {
                            $i = 0;
                            foreach ($logs as $chat) {

                                ?>

                                <div class="box-comment">
                                    <div class="comment-text">
                                    <span class="username">
                                         <i class="fa fa-comment"></i> <?= $chat->comments; ?><br/>
                                        <span class="text-muted pull-right">
                                            <?= date('D M j G:i:s', strtotime($chat->date_comment)); ?>
                                        </span>
                                    </span><br/>

                                        <p>
                                            <?php
                                            if ($chat->agenda) {
                                                ?>
                                                <strong>Agenda: </strong><?= $chat->agenda; ?><br/>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            if ($chat->date_time && $chat->date_time != "0000-00-00 00:00:00") {
                                                ?>
                                                <strong>Time: </strong><?= $chat->date_time; ?><br/>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                            if ($chat->venue) {
                                                ?>
                                                <strong>Venue: </strong><?= $chat->venue; ?><br/>
                                                <?php
                                            }
                                            ?>
                                        </p>
                                    </div>
                                    <!-- /.comment-text -->
                                </div>
                                <?php

                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    .direct-chat-messages {
        height: 600px;
        overflow: auto;
    }

    .box-comments .username {
        color: #444;
        display: block;
        font-weight: 400;
    }

    .white_bg {
        background: #ffffff;
        padding: 25px;
    }

</style>

<?= $this->registerJs('
    
    $(document).find("#myDatePicker").datepicker({});
    $(document).find("#timePicker").timepicker();
    
     
    
');

?>
