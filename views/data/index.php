<?php

use app\models\Sources;
use app\models\Status;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CsvDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data List';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="csv-data-index background_card">

        <p><?= Html::a('Create Csv Data', ['create'], ['class' => 'pull-right btn btn-success']) ?></p>

        <h1><?= Html::encode($this->title) ?></h1>

        <?php echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([

            'dataProvider' => $dataProvider,
            'columns' => [

                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $new = "";
                        if($model->isNew) {
                            $new = "<span class='label label-warning'>New</span>";
                        }
                        return $model->name .$new;
                    }
                ],
                [
                    'attribute' => 'email',
                    'format' => 'raw',
                    'value' => function ($model) {

                        $duplicates = \app\models\SubData::find()->where(['data_id' => $model->id])->count();
                        $email = Html::a($model->email, '#', ['href' => $model->email]);

                        return $email . "&nbsp;&nbsp;<span class='label label-info'>" . $duplicates . "</span>";
                    }
                ],
                'phone_number',
                'domain',
                'compnay',
                [
                    'attribute' => 'interests',
                    'format' => 'raw',
                    'value' => function ($model) {

                        $interests = explode(',', $model->interests);

                        $html = "";
                        if (count($interests) > 0) {
                            foreach ($interests as $interest) {
                                $html .= "<span class='label label-info'>" . $interest . "</span> ";
                            }
                        }
                        return $html;
                    }
                ],
                [
                    'attribute' => 'source',
                    'format' => 'raw',
                    'value' => function ($model) {

                        $sources = Sources::find()->where(['id' => $model->source])->one();
                        $_source = "";
                        if ($sources <> null) {
                            $_source = $sources->title;
                        }

                        return '<span id="source_' . $model->id . '">' . $_source . '</span>' .
                            Html::a('<i class="fa fa-pencil"></i>', '#', [
                                'class' => 'showModalButton',
                                'value' => Url::to(['/data/change-source', 'id' => $model->id, 'type' => 'main']),
                                'title' => Yii::t('yii', 'Change Source'),
                                'style' => 'padding:5px;'
                            ]);

                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {

                        $status = Status::find()->where(['id' => $model->source])->one();
                        $_status = "";

                        if ($status <> null) {
                            $_status = $status->title;
                        }

                        return '<span id="status_' . $model->id . '">' . $_status . '</span>' .
                            Html::a('<i class="fa fa-pencil"></i>', '#', [
                                'class' => 'showModalButton',
                                'value' => Url::to(['/data/change-status', 'id' => $model->id, 'type' => 'main']),
                                'title' => Yii::t('yii', 'Change Source'),
                                'style' => 'padding:5px;'
                            ]);
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'template' => '{view}{update}{delete}{logs}',
                    'buttons' => [
                        'logs' => function ($url, $model) {

                            return Html::a('<span class="label label-info"><span class="glyphicon glyphicon-comment"></span></span>', '#', [
                                'class' => 'showModalButton',
                                'value' => Url::to(['/data/save-logs', 'id' => $model->id, 'type' => 'main']),
                                'title' => Yii::t('yii', 'Logs'),
                                'id' => 'commentLog-' . $model->id
                            ]);

                        },
                    ],

                ],
            ],
        ]); ?>
    </div>


<?= $this->registerJs('
   
    /****************************************************/
    
    $("body").on("click", "#SubmitForms", function(e) {
       
        var formAction = $("#LogsForm").attr("action");
        $.ajax({
           type: "POST",
           url: formAction,
           data: $("#LogsForm").serialize(),
           success: function (data) { 
               $("body").find("#comments_logs_container").html(data);
           },
           error: function () {
                alert("Error!!!");
           }
        });
        
        e.preventDefault();
    });
    
    /****************************************************/
    
    $("body").on("click", ".change_status", function(e) {
     
        e.preventDefault(); 
        var data_id = $(this).attr("id"); 
        var status = $(this).val();
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/data/update-status/",
            type: "POST",
            data: { status: status , id : data_id, type : "main" }, 
            success : function(res){
                
                if(res){
                    $("#success").removeClass("hidden");
                    $("#status_"+ data_id).text($(".change_status option:selected").text());
                } else{
                    $("#error").removeClass("hidden");
                }
               
               setTimeout(function(){ 
                    $(".change_source_label").addClass("hidden"); 
                    $("#modal").hide();
               }, 2000);
            },
            error : function(data){
                $("#error").removeClass("hidden");
                setTimeout(function(){ $(".change_source_label").addClass("hidden"); }, 2000);
            }
        }); 
    });
    
    /****************************************************/
    
    $("body").on("click", ".change_source", function(e) {
     
        e.preventDefault(); 
        var data_id = $(this).attr("id"); 
        var source = $(this).val();
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/data/update-source/",
            type: "POST",
            data: { source: source , id : data_id}, 
            success : function(res){
                
                if(res){
                    $("#success").removeClass("hidden");
                    $("#source_"+ data_id).text($(".change_source option:selected").text());
                }else{
                    $("#error").removeClass("hidden");
                }
               
               setTimeout(function(){ 
                    $(".change_source_label").addClass("hidden"); 
                    $("#modal").hide();
               }, 2000);
            },
            error : function(data){
                $("#error").removeClass("hidden");
                setTimeout(function(){ $(".change_source_label").addClass("hidden"); }, 2000);
            }
        });
        
    });
    
    /****************************************************/
    
     
'); ?>