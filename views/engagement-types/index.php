<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EngagementTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Engagement Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="engagement-types-index background_card">

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'pull-right btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'label' => 'Status',
                'value' => function($model){
                    return ($model->status) ? "Active" : "In-active";
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
