<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EngagementTypes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="engagement-types-form background_card">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-5">
        <div class="form-group">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="col-md-5">
        <div class="form-group">
            <?= $form->field($model, 'status')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select ...']) ?>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
                    'id' => 'button',
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
            ]); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
    #button{
        margin-top: 25px;
    }
</style>