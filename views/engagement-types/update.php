<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EngagementTypes */

$this->title = 'Update Engagement Types: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Engagement Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="engagement-types-update background_card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
