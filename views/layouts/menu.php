<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\components\widgets\Nav;

echo Nav::widget([
    'items' => [
        '<li class="header">MAIN NAVIGATION</li>',
        [
            'label' => ficon('home', '<span>Home</span>'),
            'url' => ['/site/index']
        ],
        [
            'label' => ficon('upload', '<span>Import</span>'),
            'url' => ['/site/import']],
        [
            'label' => ficon('list', '<span>List All</span>'),
            'url' => ['/data']
        ],
        [
            'label' => ficon('strikethrough', '<span>Sources</span>'),
            'url' => ['/sources']
        ],
        [
            'label' => ficon('font', '<span>Status</span>'),
            'url' => ['/status']
        ],
        [
            'label' => ficon('font', '<span>Engagements</span>'),
            'url' => ['/engagement-types']
        ],

        Yii::$app->user->isGuest ? (
            [
                'label' => ficon('sign-in', '<span>Login</span>'),
                'url' => ['/site/login']
            ]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
            . Html::submitButton(
                ficon('sign-out', '<span>Logout (' . Yii::$app->user->identity->username . ')</span>'),
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>'
        )
    ],
]);
