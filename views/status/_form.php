<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Status */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="status-form background_card">


    <div class="sources-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-5">
            <div class="form-group">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <?= $form->field($model, 'active')->dropDownList(['1' => 'Active', '0' => 'In-Active'], ['prompt' => 'Select Status...']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['id' => 'button', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
<style>
    #button{
        margin-top: 25px;
    }
</style>