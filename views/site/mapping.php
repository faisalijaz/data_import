<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/14/18
 * Time: 4:06 PM
 */

use yii\widgets\ActiveForm;

$table_fields = [
    'name',
    'email',
    'phone_number',
    'domain',
    'compnay',
    'city',
    'date',
    'registration_date',
    'expiry_date',
    'server',
    'ip_address',
    'user_id',
    'country',
    'category'
];

$contents = fopen($filepath, "r");
$index = fgetcsv($contents);

?>

    <p><h2><u>Mapping</u></h2></p>

    <hr/>

<?php $form = ActiveForm::begin([
    'action' => '/site/save-data-import',
    'options' => ['data-pjax' => true]
]); ?>

    <div class="csv-data-form background_card">
        <div class="row">
            <div class="col-md-12">
                <?= \yii\helpers\Html::hiddenInput('filePath', $filepath, []); ?>
                <div class="row form-group">
                    <div class="col-md-2">
                        <label>Name: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('name', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....', 'prompt' => 'Select ....']); ?>
                    </div>

                    <div class="col-md-2">
                        <label>Email: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('email', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col-md-2">
                        <label>Phone Number: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('phone_number', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                    <div class="col-md-2">
                        <label>Domain: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('domain', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col-md-2">
                        <label>Company: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('compnay', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                    <div class="col-md-2">
                        <label>City: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('city', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-2">
                        <label>Date: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('date', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                    <div class="col-md-2">
                        <label>Registration Date: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('registration_date', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-2">
                        <label>Expiry Date: </label>
                    </div>

                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('expiry_date', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                    <div class="col-md-2">
                        <label>Server: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('server', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-2">
                        <label>IP Address: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('ip_address', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                    <div class="col-md-2">
                        <label>Country: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('country', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-2">
                        <label>Category: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('category', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>

                    <div class="col-md-2">
                        <label>User Id: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('user_id', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-2">
                        <label>Source: </label>
                    </div>
                    <div class="col-md-4">
                        <?= \yii\helpers\Html::dropDownList('source', '', $index, ['class' => 'form-control', 'prompt' => 'Select ....']); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12 text-right">
                        <?= \yii\helpers\Html::submitButton('Save', ['class' => 'btn btn-primary']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>