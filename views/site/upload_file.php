<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Upload Files';
?>
<div class="site-index background_card">

    <div class="jumbotron">
        <h1>Upload File!</h1>

        <p class="lead">Please select a file to upload</p>

    </div>

    <div class="body-content">

        <div class="row text-center">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'path')->fileInput(['class' => 'text-center form-control']); ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group text-left">
                    <button type="submit" class="btn btn-info" style="margin-top: 25px;">Import</button>
                </div>
            </div>

            <?php ActiveForm::end() ?>

        </div>
    </div>
</div>
