<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'To Do List';
?>
<div class="site-index background_card">

    <div class="counries-index background_card">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Name',
                    'format' => 'raw',
                    'value' => function ($model) {

                        if ($model->type == "main") {
                            if ($model->leadInfo <> null) {
                                return $model->leadInfo->name;
                            }
                        }

                        if ($model->type == "sub") {
                            if ($model->subLeadInfo <> null) {
                                return $model->subLeadInfo->name;
                            }
                        }
                    }
                ],
                [
                    'label' => 'Email',
                    'format' => 'raw',
                    'value' => function ($model) {

                        if ($model->type == "main") {
                            if ($model->leadInfo <> null) {
                                return Html::a($model->leadInfo->email, '#', ['href' => $model->leadInfo->email]);
                            }
                        }

                        if ($model->type == "sub") {
                            if ($model->subLeadInfo <> null) {
                                return Html::a($model->subLeadInfo->email, '#', ['href' => $model->subLeadInfo->email]);
                            }
                        }
                    }
                ],
                [
                    'label' => 'Phone Number',
                    'format' => 'raw',
                    'value' => function ($model) {

                        if ($model->type == "main") {
                            if ($model->leadInfo <> null) {
                                return $model->leadInfo->phone_number;
                            }
                        }

                        if ($model->type == "sub") {
                            if ($model->subLeadInfo <> null) {
                                return $model->subLeadInfo->phone_number;
                            }
                        }
                    }
                ],
                'agenda',
                'date_time',
                'venue',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>


</div>