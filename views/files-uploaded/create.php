<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FilesUploaded */

$this->title = Yii::t('app', 'Create Files Uploaded');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Files Uploadeds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="files-uploaded-create background_card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
