<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FilesUploadedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="files-uploaded-index background_card">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'path',
            [
                'attribute' => 'imported',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->imported) ? "<span class='label label-success'>Yes</span>" : "<span class='label label-warning'>No</span>";
                },
                'filter' => ["1" => "Yes", "0" => "No"],
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'Select ...'],
            ],
            'date_add',
            'date_import',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?></div>
