<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logs;

/**
 * LogsSearch represents the model behind the search form about `app\models\Logs`.
 */
class LogsSearch extends Logs
{
    /**
     * @inheritdoc
     */

    public $current_date = false;

    public function rules()
    {
        return [
            [['id', 'data_id', 'user_id'], 'integer'],
            [['current_date','comments', 'date_comment', 'type', 'agenda', 'date_time', 'venue'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Logs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data_id' => $this->data_id,
            'date_comment' => $this->date_comment,
            'user_id' => $this->user_id,
            'date_time' => $this->date_time,
        ]);

        $query->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'agenda', $this->agenda])
            ->andFilterWhere(['like', 'venue', $this->venue]);

        if($this->current_date){
            $query->andFilterWhere(['<=', 'date_time', date('Y-m-d')]);
            $query->andFilterWhere(['>=', 'date_time', date('Y-m-d')]);
        }

        return $dataProvider;
    }
}
