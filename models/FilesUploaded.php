<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files_uploaded".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property string $imported
 * @property string $date_add
 * @property string $date_import
 */
class FilesUploaded extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'files_uploaded';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['imported'], 'string'],
            [['date_add', 'date_import', 'date_import', 'file'], 'safe'],
            [['title', 'path'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'path' => Yii::t('app', 'Path'),
            'imported' => Yii::t('app', 'Imported'),
            'date_add' => Yii::t('app', 'Date Add'),
            'date_import' => Yii::t('app', 'Date Import'),
        ];
    }
}
