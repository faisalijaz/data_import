<?php
namespace app\models;

use ruskid\csvimporter\CSVImporter;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/13/18
 * Time: 1:05 PM
 */


class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'csv,xlsx,xls'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $this->file->baseName . '.' . $this->file->extension);
            return true;
        } else {
            return false;
        }
    }
}