<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "csv_data".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone_number
 * @property string $domain
 * @property string $compnay
 * @property string $city
 * @property string $date
 * @property string $registration_date
 * @property string $expiry_date
 * @property string $server
 * @property string $ip_address
 * @property integer $user_id
 * @property integer $country
 * @property string $category
 */
class CsvData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csv_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'unique', 'targetAttribute' => ['email'], 'message' => 'Email must be unique.'],
            ['phone_number', 'unique', 'targetAttribute' => ['phone_number'], 'message' => 'Phone number must be unique.'],
            [['date', 'registration_date', 'expiry_date','interests','user_id', 'country'], 'safe'],
            [['isNew','name', 'email', 'phone_number', 'source','domain', 'compnay', 'city', 'server', 'ip_address', 'category'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'domain' => Yii::t('app', 'Domain'),
            'compnay' => Yii::t('app', 'Compnay'),
            'city' => Yii::t('app', 'City'),
            'date' => Yii::t('app', 'Date'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'expiry_date' => Yii::t('app', 'Expiry Date'),
            'server' => Yii::t('app', 'Server'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'user_id' => Yii::t('app', 'User ID'),
            'country' => Yii::t('app', 'Country'),
            'category' => Yii::t('app', 'Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubData()
    {
        return $this->hasMany(SubData::className(), ['data_id' => 'id']);
    }

}
