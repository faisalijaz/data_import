<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logs".
 *
 * @property integer $id
 * @property integer $data_id
 * @property string $comments
 * @property string $date_comment
 * @property integer $user_id
 * @property string $type
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    public $time;
    public $tagsInput;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_id', 'comments', 'type'], 'required'],
            [['data_id', 'user_id'], 'integer'],
            [['comments', 'type', 'agenda', 'venue'], 'string'],
            [['date_comment', 'interests','date_time', 'time', 'tagsInput'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data_id' => Yii::t('app', 'Data ID'),
            'comments' => Yii::t('app', 'Comments'),
            'date_comment' => Yii::t('app', 'Date Comment'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {

        if (!empty($this->tagsInput)) {

            if ($this->type && $this->type == "main") {

                $main = CsvData::find()->where(['id' => $this->data_id])->one();
                if ($main <> null) {
                    $main->interests = $this->tagsInput;
                    $main->save();
                }
            }

            if ($this->type && $this->type == "sub") {

                $sub = SubData::find()->where(['id' => $this->data_id])->one();
                if ($sub <> null) {
                    $sub->interests = $this->tagsInput;
                    $sub->save();
                }
            }
        }

        $date_time = $this->date_time . $this->time;
        $this->date_time = date('Y-m-d H:i:s', strtotime($date_time));

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadInfo()
    {
        return $this->hasOne(CsvData::className(), ['id' => 'data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubLeadInfo()
    {
        return $this->hasOne(SubData::className(), ['id' => 'data_id']);
    }
}
