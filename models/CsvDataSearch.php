<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CsvDataSearch represents the model behind the search form about `app\models\CsvData`.
 */
class CsvDataSearch extends CsvData
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [[ 'isNew', 'user_id', 'country', 'name', 'source', 'status', 'email', 'phone_number', 'domain', 'compnay', 'city', 'date', 'registration_date', 'expiry_date', 'server', 'ip_address', 'category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsvData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'registration_date' => $this->registration_date,
            'expiry_date' => $this->expiry_date,
            'user_id' => $this->user_id,
            'country' => $this->country,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'domain', $this->domain])
            ->andFilterWhere(['like', 'compnay', $this->compnay])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'server', $this->server])
            ->andFilterWhere(['like', 'ip_address', $this->ip_address])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'source', $this->source]);

        return $dataProvider;
    }
}
